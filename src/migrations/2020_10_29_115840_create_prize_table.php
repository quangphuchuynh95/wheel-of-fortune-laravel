<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prize', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('prize_category_id');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->json('user_info')->nullable();
            $table->boolean('already_owned')->default(FALSE);
            $table->timestamps();

            $table->index('prize_category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prize');
    }
}
