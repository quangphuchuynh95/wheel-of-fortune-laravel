<?php

namespace QuangPhuc\WheelOfFortuneLaravel;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use QuangPhuc\WheelOfFortuneLaravel\Model\Prize;
use QuangPhuc\WheelOfFortuneLaravel\Model\PrizeCategory;
use QuangPhuc\WheelOfFortuneLaravel\Model\SpinLink;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\CreateRequest;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\UpdateRequest;

class WheelOfFortuneProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $binding = [
            [
                \QuangPhuc\WheelOfFortuneLaravel\Controllers\Back\PrizeCategoryController::class,
                \QuangPhuc\WheelOfFortuneLaravel\Request\Back\PrizeCategory\CreateRequest::class,
                \QuangPhuc\WheelOfFortuneLaravel\Request\Back\PrizeCategory\UpdateRequest::class,
            ],
        ];
        $this->app->bind(CreateRequest::class, function (\Illuminate\Foundation\Application $app , $b) {
            $action = $app->get('request')->route()->getAction()['controller'];
            list($controller, $method) = explode('@', $action);
            $controllerNameSpaces = explode('\\', $controller);
            $controllerName = $controllerNameSpaces[count($controllerNameSpaces) - 1];
            $modelName = str_replace('Controller', '', $controllerName);
            return $app->make("QuangPhuc\\WheelOfFortuneLaravel\\Request\\Back\\{$modelName}\\CreateRequest");
        });
        $this->app->bind(UpdateRequest::class, function (\Illuminate\Foundation\Application $app , $b) {
            $action = $app->get('request')->route()->getAction()['controller'];
            list($controller, $method) = explode('@', $action);
            $controllerNameSpaces = explode('\\', $controller);
            $controllerName = $controllerNameSpaces[count($controllerNameSpaces) - 1];
            $modelName = str_replace('Controller', '', $controllerName);
            return $app->make("QuangPhuc\\WheelOfFortuneLaravel\\Request\\Back\\{$modelName}\\UpdateRequest");
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Route::model('spin-link', SpinLink::class);
        Route::model('prize', Prize::class);
        Route::model('prize-category', PrizeCategory::class);

        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->publishes([
            __DIR__.'/config.php' => config_path('fortune_wheel.php'),
        ], 'config');
        $this->loadViewsFrom(__DIR__.'/resource/view', 'fortune_wheel');
    }
}
