<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/**
 * Class Prize
 * @package QuangPhuc\WheelOfFortuneLaravel\Model
 * @property string $id
 * @property string $prize_category_id
 * @property string $phone
 * @property string $email
 * @property array $user_info
 * @property boolean $already_owned
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Prize extends Model
{
    protected $table = 'prize';
    protected $fillable = ['prize_category_id', 'email', 'phone', 'user_info', 'already_owned'];

    protected $keyType = 'string';
    public $incrementing = false;
    protected $casts = [
        'user_info' => 'array',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Uuid::uuid4()->toString());
        });
    }
    /**
     * @task
     * @since 10/29/20
     * @author quang_phuc
     * @throws \Exception
     */
    public static function generate() {
        try {
            DB::beginTransaction();
            $categories = PrizeCategory::all();
            $faker = new Faker();
            foreach ($categories as $category) {
                for ($i = 1; $i <= $category->probability; $i ++) {
                    $prize = [
                        'prize_category_id' => $category->id,
                        'created_at' => Carbon::now()->toDateTimeString(),
                    ];
                    Prize::query()->create($prize);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function category() {
        return $this->belongsTo(PrizeCategory::class, 'prize_category_id');
    }
}
