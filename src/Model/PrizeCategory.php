<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Str;

/**
 * Class PrizeCategory
 * @package QuangPhuc\WheelOfFortuneLaravel\Model
 *
 * @property string $id
 * @property string $name
 * @property integer $probability
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class PrizeCategory extends Model
{
    protected $table = 'prize_category';
    protected $guarded = [];
    protected $fillable = ['name', 'probability'];

    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid());
        });
    }

    public function prizes() {
        return $this->hasMany(Prize::class, 'prize_category_id');
    }
}
