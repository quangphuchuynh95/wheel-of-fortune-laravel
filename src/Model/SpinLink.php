<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Str;

/**
 * Class SpinLink
 * @package QuangPhuc\WheelOfFortuneLaravel\Model
 *
 * @property string $id
 * @property integer $total_time
 * @property integer $remain_time
 * @property integer $email
 * @property integer $phone
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class SpinLink extends Model
{
    protected $table = 'spin_link';
    protected $guarded = [];
    protected $fillable = ['email', 'phone', 'total_time', 'remain_time', 'name'];

    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid());
        });
    }
}
