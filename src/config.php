<?php

return [
    'middleware' => [
        'back' => [
            // example: 'auth', ....
        ],
        'front' => [
            // example: 'auth', ....
        ],
    ],
    "chances_per_session" => -1,
    'url_prefix' => '/fortune-wheel',
    'view' => [
        'front' => 'fortune_wheel::index',
        'back' => [
            'prize' => 'fortune_wheel::back.prize',
            'prize-category' => 'fortune_wheel::back.prize-category',
            'spin-link' => 'fortune_wheel::back.spin-link',
        ]
    ],
];
