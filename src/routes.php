<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['web'])->group(function () {
    Route::prefix('/back')->group(function () {
        Route::resource('prize', \QuangPhuc\WheelOfFortuneLaravel\Controllers\Back\PrizeController::class);
        Route::get('prize/delete/{prize}', '\QuangPhuc\WheelOfFortuneLaravel\Controllers\Back\PrizeController@destroy')->name('prize.delete');
        Route::resource('prize_category', \QuangPhuc\WheelOfFortuneLaravel\Controllers\Back\PrizeCategoryController::class);
        Route::resource('spin_link', \QuangPhuc\WheelOfFortuneLaravel\Controllers\Back\SpinLinkController::class);
        Route::get('spin_link/delete/{spin_link}', '\QuangPhuc\WheelOfFortuneLaravel\Controllers\Back\SpinLinkController@destroy')->name('spin_link.delete');

    });

    Route::prefix(config('fortune_wheel.url_prefix'))->namespace('\QuangPhuc\WheelOfFortuneLaravel\Controllers\Front')->group(function () {
        Route::get('/', 'FortuneWheelController@index')->name('spin.index');
        Route::get('result', 'FortuneWheelController@spin')->name('spin.result');
        Route::post('register-information', 'FortuneWheelController@registerInformation')->name('spin.information');
        Route::get('/{spin_link}', 'FortuneWheelController@index')->name('spin.index.link');
    });
});
