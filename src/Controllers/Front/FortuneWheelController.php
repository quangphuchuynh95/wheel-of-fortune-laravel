<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Controllers\Front;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Request;
use Illuminate\View\View;
use QuangPhuc\WheelOfFortuneLaravel\Controllers\Controller;
use QuangPhuc\WheelOfFortuneLaravel\Model\Prize;
use QuangPhuc\WheelOfFortuneLaravel\Model\SpinLink;
use QuangPhuc\WheelOfFortuneLaravel\Request\Front\RegisterInformationRequest;

class FortuneWheelController extends Controller {
    /**
     * Show fortune wheel page
     *
     * @param SpinLink $spinLink
     *
     * @return Factory|Application|View
     * @throws \Exception
     * @author quang_phuc
     * @since 10/29/20
     */
    public function index(SpinLink $spinLink = NULL) {
        if ($spinLink) {
            session(['spin-link' => $spinLink->id]);
        } elseif ($id = session('spin-link')) {
            $spinLink = SpinLink::query()->find($id);
        }
        $countRemainPrizes = Prize::query()->where('already_owned', FALSE)->count();
        if (!$countRemainPrizes) {
            Prize::generate();
        }
        return view(config('fortune_wheel.view.front'), compact('spinLink'));
    }

    /**
     * Return spin result for user
     * @throws \Exception
     * @author quang_phuc
     * @since 10/29/20
     * @return Response|ResponseFactory
     */
    public function spin() {
        $countRemainPrizes = Prize::query()->where('already_owned', FALSE)->count();
        if (!$countRemainPrizes) {
            Prize::generate();
        }
        /**
         * @var SpinLink $spinLink
         */
        $spinLink = null;
        if ($spinLinkId = session('spin-link')) {
            $spinLink = SpinLink::query()->find($spinLinkId);
        }

        if ($spinLink) {
            if ($spinLink->remain_time <= 0) {
                $message = 'Bạn đã hết số lần quay';
                return response(compact('message'), 400);
            }
            $prize = Prize::query()->where('already_owned', FALSE)->inRandomOrder()->first();
            $prize->update([
                'already_owned' => TRUE,
                'email' =>  $spinLink->email,
                'phone' => $spinLink->phone,
            ]);
            $spinLink->decrement('remain_time');
            session(['previous_prize' => $prize->id]);
            return response(
                [
                    'prize' => $prize->toArray(),
                    'is_new' => TRUE,
                    'spin_link' => $spinLink,
                ]
            );
        }
        if (($prizeId = session('previous_prize')) && (config('fortune_wheel.chances_per_session') != -1)) {
            $prize = Prize::query()->find($prizeId);
            return response(
                [
                    'prize' => $prize->toArray(),
                    'is_new' => FALSE
                ]
            );
        }
        $prize = Prize::query()->where('already_owned', FALSE)->inRandomOrder()->first();
        $prize->update(['already_owned' => TRUE]);
        session(['previous_prize' => $prize->id]);
        return response(
            [
                'prize' => $prize->toArray(),
                'is_new' => TRUE
            ]
        );
    }

    /**
     * @param RegisterInformationRequest $request
     *
     * @task Save user information
     * @return ResponseFactory|Application|Response
     * @author quang_phuc
     * @since 10/29/20
     */
    public function registerInformation(RegisterInformationRequest $request) {
        /**
         * @var Prize $prize
         */
        $prize = null;
        if ($prizeId = session('previous_prize')) {
            $prize = Prize::query()->find($prizeId);
        }

        if (!$prize) {
            return response([
                'status' => FALSE,
                'message' => 'There is no prize',
            ], 400);
        }

        $prize->update([
            'phone' => $request->phone,
            'email' => $request->email,
            'user_info' => [
                'name' => $request->name,
            ],
        ]);

        return response([
            'status' => TRUE,
        ], 200);
    }
}
