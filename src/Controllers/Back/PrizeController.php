<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Controllers\Back;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Request;
use QuangPhuc\WheelOfFortuneLaravel\Controllers\Controller;
use QuangPhuc\WheelOfFortuneLaravel\Model\Prize;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\Prize\CreateRequest;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\Prize\UpdateRequest;

class PrizeController extends BackController {
    public function getModel() {
        return new Prize();
    }

    function getViewFolder() {
        return config('fortune_wheel.view.back.prize');
    }
}
