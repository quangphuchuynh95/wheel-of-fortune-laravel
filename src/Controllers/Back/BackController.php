<?php
/**
 * BackController.php
 * ${CARET}
 *
 * [] - [quang_phuc] - [10/29/20]
 */

namespace QuangPhuc\WheelOfFortuneLaravel\Controllers\Back;


use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use QuangPhuc\WheelOfFortuneLaravel\Controllers\Controller;
use QuangPhuc\WheelOfFortuneLaravel\Model\Prize;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\CreateRequest;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\UpdateRequest;
use Illuminate\Database\Eloquent\Model;

abstract class BackController extends Controller {
    /**
     * @return Model
     * @since 10/29/20
     * @author quang_phuc
     */
    abstract function getModel();

    /**
     * @return Builder
     * @since 10/29/20
     * @author quang_phuc
     */
    public function getQuery() {
        return $this->getModel()->newQuery();
    }

    /**
     * @return string
     * @since 10/29/20
     * @author quang_phuc
     */
    abstract function getViewFolder();

    /**
     * @param string $action
     *
     * @return string
     * @since 10/29/20
     * @author quang_phuc
     */
    public function getView($action) {
        return $this->getViewFolder() . '.' . $action;
    }

    function __construct() {
        $this->middleware(config('fortune_wheel.middleware.back'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param CreateRequest $request
     *
     * @return Factory|Application|View|void
     */
    public function index(CreateRequest $request)
    {
        return view($this->getView('index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|Application|Response|View
     */
    public function create()
    {
        return view($this->getView('create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRequest  $request
     *
     * @return array|RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        $model = $this->getQuery()->create($request->only($this->getModel()->getFillable()));
        if ($request->expectsJson()) {
            return $model->toArray();
        }
        return redirect()->back()->with('success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param Model $model
     *
     * @return Factory|Application|View|void
     */
    public function show(Model $model)
    {
        return view($this->getView('show'), compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Model $model
     *
     * @return Factory|Application|View
     */
    public function edit(Model $model)
    {
        return view($this->getView('edit'), compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Model         $model
     *
     * @return RedirectResponse|array
     */
    public function update(UpdateRequest $request, Model $model)
    {
        $model->update($request->only($this->getModel()->getFillable()));
        if ($request->expectsJson()) {
            return $model->toArray();
        }
        return redirect()->back()->with('success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Model $model
     *
     * @return bool
     * @throws \Exception
     */
    public function destroy(Model $model)
    {
        $model->delete();
        return redirect()->back();
    }
}
