<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Controllers\Back;

use Illuminate\Support\Facades\Request;
use QuangPhuc\WheelOfFortuneLaravel\Controllers\Controller;
use QuangPhuc\WheelOfFortuneLaravel\Model\Prize;
use QuangPhuc\WheelOfFortuneLaravel\Model\PrizeCategory;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\PrizeCategory\CreateRequest;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\PrizeCategory\UpdateRequest;

class PrizeCategoryController extends BackController {
    public function getModel() {
        return new PrizeCategory;
    }

    function getViewFolder() {
        return config('fortune_wheel.view.back.prize-category');
    }
}
