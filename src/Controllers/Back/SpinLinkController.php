<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Controllers\Back;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Request;
use QuangPhuc\WheelOfFortuneLaravel\Controllers\Controller;
use QuangPhuc\WheelOfFortuneLaravel\Model\Prize;
use QuangPhuc\WheelOfFortuneLaravel\Model\SpinLink;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\SpinLink\CreateRequest;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\SpinLink\UpdateRequest;

class SpinLinkController extends BackController {
    public function getModel() {
        return new SpinLink();
    }

    function getViewFolder() {
        return config('fortune_wheel.view.back.spin-link');
    }
}
