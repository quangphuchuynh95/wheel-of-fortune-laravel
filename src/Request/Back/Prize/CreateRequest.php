<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Request\Back\Prize;

use Illuminate\Foundation\Http\FormRequest;
use QuangPhuc\WheelOfFortuneLaravel\Request\Back\CreateRequest as BaseCreateRequest;

class CreateRequest extends BaseCreateRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
