<?php

namespace QuangPhuc\WheelOfFortuneLaravel\Request\Front;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RegisterInformationRequest
 * @package QuangPhuc\WheelOfFortuneLaravel\Request\Front
 *
 * @property string $name
 * @property string $email
 * @property string $phone
 */
class RegisterInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !empty(session('previous_prize'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
